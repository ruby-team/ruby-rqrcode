ruby-rqrcode (2.2.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Standards-Version: 4.6.2 (routine-update)
  * Reorder sequence of d/control fields by cme (routine-update)
  * Refresh patches
  * Remove X?-Ruby-Versions fields from d/control
  * Remove standardrb gem from gemspec dependencies

 -- Pirate Praveen <praveen@debian.org>  Sat, 03 Feb 2024 23:57:18 +0530

ruby-rqrcode (1.2.0-1) unstable; urgency=medium

  [ Debian Janitor ]
  * Update watch file format version to 4.
  * Remove constraints unnecessary since buster:
    + Build-Depends: Drop versioned constraint on ruby-chunky-png.
    + ruby-rqrcode: Drop versioned constraint on ruby-chunky-png in Depends.

  [ Gunnar Wolf ]
  * Removing myself from uploaders, acknowledging reality

  [ Cédric Boutillier ]
  * New upstream version 1.2.0
    + remove upper bound restriction on ruby version in gemspecs
  * Refresh packaging files with dh-make-ruby -w
    + add upstream metadata
    + Standards-Version to 4.6.0 (no changes needed)
    + debhelper compat level set to 13
  * Add myself to uploaders
  * Run tests with rake
  * Build-depend on ruby-rspec

 -- Cédric Boutillier <boutil@debian.org>  Wed, 23 Feb 2022 18:08:49 +0100

ruby-rqrcode (1.1.2-3) unstable; urgency=medium

  * Team upload.
  * Reupload to unstable
  * Add Breaks: ruby-qr4r (<= 0.4.1-1~)
  * Move debian/watch to gemwatch.debian.net

 -- Pirate Praveen <praveen@debian.org>  Thu, 08 Oct 2020 10:10:42 +0530

ruby-rqrcode (1.1.2-2) experimental; urgency=medium

  * Team upload.
  * Check dependencies during build
  * Add missing (build) dependencies

 -- Pirate Praveen <praveen@debian.org>  Wed, 16 Sep 2020 17:44:24 +0530

ruby-rqrcode (1.1.2-1) experimental; urgency=medium

  [ Cédric Boutillier ]
  * Remove version in the gem2deb build-dependency
  * Use https:// in Vcs-* fields
  * Use https:// in Vcs-* fields
  * Bump Standards-Version to 3.9.7 (no changes needed)
  * Run wrap-and-sort on packaging files

  [ Utkarsh Gupta ]
  * Add salsa-ci.yml

  [ Debian Janitor ]
  * Trim trailing whitespace.
  * Use secure copyright file specification URI.
  * Use secure URI in Homepage field.
  * Bump debhelper from old 9 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Update Vcs-* headers from URL redirect.
  * Use canonical URL in Vcs-Git.

  [ Cédric Boutillier ]
  * Update team name
  * Add .gitattributes to keep unwanted files out of the source package

  [ Pirate Praveen ]
  * New upstream version 1.1.2
  * Bump Standards-Version to 4.5.0 (no changes needed)
  * Remove git usage in gemspec
  * Disable tests (no longer shipped with gem)

 -- Pirate Praveen <praveen@debian.org>  Sat, 12 Sep 2020 23:31:50 +0530

ruby-rqrcode (0.4.2-3) unstable; urgency=medium

  * Team upload.
  * Rebuild to generate gemspec for newer rubies/all
  * Bump compat to 9
  * Bump standards version to 3.9.6 (no changes)

 -- Pirate Praveen <praveen@debian.org>  Fri, 02 Oct 2015 10:12:35 +0530

ruby-rqrcode (0.4.2-2) unstable; urgency=low

  * Team upload.

  [ Cédric Boutillier ]
  * use canonical URI in Vcs-* fields
  * debian/copyright: use DEP5 copyright-format/1.0 URL for Format field

  [ Jonas Genannt ]
  * d/control
    - removed transitional packages
    - bumped standards version to 3.9.5 (no changes needed)
    - removed version dependency of ruby1.8, changed to ruby

 -- Jonas Genannt <jonas.genannt@capi2name.de>  Sat, 30 Nov 2013 21:40:22 +0100

ruby-rqrcode (0.4.2-1) unstable; urgency=low

  * New upstream version

 -- Gunnar Wolf <gwolf@debian.org>  Sun, 15 Jan 2012 11:46:21 -0600

ruby-rqrcode (0.3.3-2) unstable; urgency=low

  * Repackaged following new Gem2deb infrastructure

 -- Gunnar Wolf <gwolf@debian.org>  Sat, 14 May 2011 20:56:05 -0500

librqrcode-ruby (0.3.3-1) unstable; urgency=low

  * New upstream release
  * Standards-version 3.8.4→3.9.1.0 (no changes needed)

 -- Gunnar Wolf <gwolf@debian.org>  Tue, 08 Feb 2011 19:57:46 -0600

librqrcode-ruby (0.3.2-3) unstable; urgency=low

  * Port the package to Ruby 1.9.1 (Closes: #569849)
  * Standards-version 3.8.3→3.8.4 (no changes needed)

 -- Gunnar Wolf <gwolf@debian.org>  Mon, 15 Feb 2010 17:52:41 -0600

librqrcode-ruby (0.3.2-2) unstable; urgency=low

  * Add myself to Uploaders
  * Debian Policy 3.8.3
  * add build dep on ruby1.9 (Closes: #543039)

 -- Ryan Niebur <ryanryan52@gmail.com>  Sun, 23 Aug 2009 12:11:18 -0700

librqrcode-ruby (0.3.2-1) unstable; urgency=low

  * Initial upload (Closes: #534769)

 -- Gunnar Wolf <gwolf@debian.org>  Mon, 29 Jun 2009 11:56:16 -0500
